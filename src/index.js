import $ from 'jQuery';
import './js/color-picker';

var getNumColors = (function() {
    return $("#color-list li").length;
});

$('body').on("click", "#add-color", function(e) {
    e.preventDefault();

    var $elem = $("#color-list li:eq(0)").clone();

    $("input", $elem).val("");
    $(".color-swatch", $elem).css('background-color', '');

    $('#color-list').append($elem);
    $('#remove-color').prop("disabled", false);

    $("input", $elem).eq(0).focus();
});

$('body').on("click", "#remove-color", function(e) {
    e.preventDefault();

    var numColors = getNumColors()

    if(numColors > 2) {
        var elem = $("#color-list li:last-child").remove();
    } else {
        alert("You must enter at least 2 colors to test.");
    }

    if(numColors == 3) {
        $('#remove-color').prop("disabled", true);
    }
});


$('body').on('keyup change', '.input--color', function(e) {
    var $this = $(this),
        $swatch = $this.next('.color-swatch');

    $swatch.css('background-color', $this.val());
});