<section class="section">
    <form action="/process.php" method="post" class="color-form">

        <ol class="color-list" id="color-list">
            <?php for($i=0;$i<2;$i++): ?>
            <li tabindex="-1">
                <fieldset>
                    <div class="grid">
                        <label>
                            <span>Name</span>

                            <input class="input" type="text" name="title[]" required>
                        </label>

                        <label>
                            <span>Colour</span>

                            <input class="input input--color" type="text" name="color[]" pattern="^#([A-Fa-f0-9]{3}){1,2}$" placeholder="#000000" required>
                            <span role="presentation" class="color-swatch"></span>
                        </label>
                    </div>
                </fieldset>
            </li>
            <?php endfor; ?>
        </ol>

        <menu class="form-controls">
            <button type="button" id="add-color" aria-controls="color-list" class="button button--small button--add">Add Colour</button>
            <button type="button" id="remove-color" aria-controls="color-list" class="button button--small button--remove" disabled>Remove Colour</button>
        </menu>

        <button type="submit" class="button">Test Colours</button>
    </form>
</section>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<?php $file = dirname(__DIR__).'/www/static/js/app.min.js'; ?>
<script src="/static/js/app.min.<?= filemtime($file); ?>.js"></script>