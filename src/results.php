<?php

require dirname(__DIR__).'/vendor/autoload.php';

use Dorigo\ColorTester\{ColorTester, Color, Levels};

function error($message = "Something went wrong. Please try again.", $code = 500) {
    http_response_code($code);
    echo $message;
    die();
}

if(isset($parts) && $parts) {
    $filename = $parts[0];
    $file = dirname(__DIR__)."/cache/{$filename}.json";

    if(!file_exists($file)) {
        error("Error: Could not find the requested colors.", 404);
    }

    $json = file_get_contents($file);
    $colors = json_decode($json, true);

    if(!$colors) {
        error("Error: There was a problem parsing your colors.", 500);
    }
} else {
    error("Error: Could not find the requested colors.", 404);
}



$numCombinations = count($colors) * (count($colors) - 1);
$fails = [];
$passes = [];

ob_start();
?>
<?php foreach($colors as $bgName => $bg): ?>
<section class="section">
    <?php
        $bg = new Color($bg);
        $bgTest = new ColorTester($bg);
    ?>
    <h2><?= $bgName; ?> Background</h2>

    <ul style="margin: 0 0 3em; padding: 0; list-style: none;">
        <?php foreach($colors as $fgName => $fg): if($fgName===$bgName) continue; ?>
        <li style="margin-bottom: 1em;">
            <?php
                $fg = new Color($fg);
                $ratio = $bgTest->test($fg);

                if(!$ratio->levelAALarge) {
                    $fails[] = $bgName.$fgName;
                }

                if($ratio->levelAAASmall) {
                    $passes[] = $bgName.$fgName;
                }
            ?>
            <table class="results" border="0">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Small Text</th>
                        <th>Large Text</th>
                        <th class="results__item__ratio">Ratio</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="results__item">
                        <th rowspan="2" scope="row" class="results__item__color
                                                          <?= $bg->luma() > 0.5 ?' results__item__color--light':'';?>
                                                          <?= $ratio->ratio < 3 ?' results__item__color--inaccessible':'';?>"
                              style="background:<?= $bg->hex(); ?>;color:<?=$fg->hex();?>;">
                            <h3 style="margin: 0; width: 200px;">
                                <?= $fgName; ?> Text
                            </h3>
                        </th>
                        <td><span class="results__item__tag results__item__tag--<?=$ratio->levelAASmall?"pass":"fail";?>">AA <?= $ratio->levelAASmall ? "Pass" : "Fail"; ?></span></td>
                        <td><span class="results__item__tag results__item__tag--<?=$ratio->levelAALarge?"pass":"fail";?>">AA <?= $ratio->levelAALarge ? "Pass" : "Fail"; ?></span></td>
                        <td rowspan="2" class="results__item__ratio">
                            <?= number_format($ratio->ratio, 2); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="results__item__tag results__item__tag--<?=$ratio->levelAAASmall?"pass":"fail";?>">AAA <?= $ratio->levelAAASmall ? "Pass" : "Fail"; ?></span></td>
                        <td><span class="results__item__tag results__item__tag--<?=$ratio->levelAAALarge?"pass":"fail";?>">AAA <?= $ratio->levelAAALarge ? "Pass" : "Fail"; ?></span></td>
                    </tr>
                </tbody>
            </table>
        </li>
        <?php endforeach; ?>
    </ul>
</section>
<?php endforeach; $tables = ob_get_clean(); ?>

<?php if($fails || $passes): ?>
<section class="section">
    <h2>Information</h2>

    <ul class="result-information">
    <?php if($fails): ?>
        <li><strong><?= count($fails); ?> out of <?= $numCombinations; ?></strong> color combinations <em class="result-information__tag result-information__tag--fail">Fail</em> all tests.</li>
    <?php endif; ?>

    <?php if($passes): ?>
        <li><strong><?= count($passes); ?> out of <?= $numCombinations; ?></strong> color combinations <em class="result-information__tag result-information__tag--pass">Pass</em> all tests.</li>
    <?php endif; ?>
    </ul>
</section>
<?php endif; ?>

<?= $tables; ?>

<section class="section">
    <p class="article">
        <a href="/" class="button">Start Over</a>
    </p>
</section>