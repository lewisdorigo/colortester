<?php

require dirname(__DIR__).'/vendor/autoload.php';

use Dorigo\ColorTester\{ColorTester, Color, Levels};


$titles = isset($_REQUEST["title"]) ? $_REQUEST["title"] : [];
$color = isset($_REQUEST["color"]) ? $_REQUEST["color"] : [];

if(!$titles || !$color || count($titles) !== count($color)) {
    echo "Something went wrong. Please try again.";
    die();
}

$colors = [];

function sanitize(string $string) {
    $string = htmlspecialchars($string);
    return strip_tags($string);
}

foreach($titles as $i => $title) {
    $colors[sanitize($title)] = sanitize($color[$i]);
}

$json = json_encode($colors);
$hash = hash('adler32', $json);
$cacheFile = dirname(__DIR__)."/cache/{$hash}.json";

if(!file_exists($cacheFile)) {
    file_put_contents($cacheFile, $json);
}

http_response_code(303);



$url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
header("Location: {$url}/results/{$hash}/");
die();