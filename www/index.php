<?php if($_GET['action'] === 'test') { require_once './process.php'; die(); } ?>
<!doctype html>
<html lang="en-GB" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <meta name="apple-mobile-web-app-title" content="Colour Tester">
    <meta name="application-name" content="Colour Tester">

    <title>WCAG Colour Tester</title>

    <?php $file = dirname(__DIR__).'/www/static/css/app.min.css'; ?>
    <link rel="stylesheet" type="text/css" href="/static/css/app.min.<?= filemtime($file); ?>.css">
</head>

<body>
    <section class="section">
        <article class="article">
            <h1>WCAG Colour Tester</h1>

            <p>The Web Content Accessibility Guidelines, or WCAG, has standards for how perceivable web-based content should be — including colour contrast.</p>
            <p>The guidelines are split into 3 levels — A, AA, and AAA — which each have different requirements.</p>
            <p>For Level AA, text and its background should have a contrast ratio of at least 4.5:1, and 3:1 for large text — defined as text that is at least 18pt (or 14pt bold). Level AA also requires a minimum contrast ratio of 3:1 for user interface components or graphical objects.</p>
            <p>For Level AAA, a contrast ratio of at least 7:1 for regular text, and 4.5:1 is required.</p>
        </article>
    </section>

<?php

$uri = isset($_SERVER['SCRIPT_URL']) && $_SERVER['SCRIPT_URL'] ? $_SERVER['SCRIPT_URL'] : null;
$uri = trim($uri, '/');

$parts = explode('/', $uri);

$action = is_array($parts) && $parts ? array_shift($parts) : null;

if(!$action) {
    $action = isset($_GET["action"]) ? $_GET["action"] : null;
    $action = $action === 'test' ? 'results' : $action;
}

switch($action) {
    case "results":
        require_once "../src/results.php";
        break;
    default:
        require_once "../src/form.php";
        break;
}
?>
</body>
</html>