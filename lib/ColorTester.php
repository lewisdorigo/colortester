<?php namespace Dorigo\ColorTester;

use Dorigo\ColorTester\{Color, Levels};

class ColorTester {
    private $color;
    private $luma;

    public function __construct(Color $color) {
        $this->color = $color;
        $this->luma = $color->luma();
    }

    public function test(Color $secondary) {
        $l1 = $this->luma;
        $l2 = $secondary->luma();

        $ratio = $l1 > $l2 ? (($l1 + 0.05) / ($l2 + 0.05)) : (($l2 + 0.05) / ($l1 + 0.05));

        return new Levels($ratio);
    }
}