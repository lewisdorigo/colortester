<?php namespace Dorigo\ColorTester;

class Levels {
    public $levelAASmall;
    public $levelAALarge;
    public $levelAAUI;

    public $levelAAASmall;
    public $levelAAALarge;

    private $currentLevel = 0;

    public function __construct(float $ratio) {
        $this->levelAASmall = $ratio >= 4.5;
        $this->levelAALarge = $ratio >= 3.0;

        $this->levelAAIUI = $ratio >= 3.0;

        $this->levelAAASmall = $ratio >= 7.0;
        $this->levelAAALarge = $ratio >= 4.5;

        $this->ratio = $ratio;
    }
}