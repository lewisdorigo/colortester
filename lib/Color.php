<?php namespace Dorigo\ColorTester;

class Color extends \stdClass {
    private $hex;
    private $luma;
    private $rgb = [];

    public $r = 0;
    public $g = 0;
    public $b = 0;

    public function __construct(string $hex) {
        $this->hex = $hex;
        $this->parseRGB();
    }

    private function parseRGB() {
        $values = str_replace("#", "", $this->hex);

        switch(strlen($values)) {
            case 3;
                list($r, $g, $b) = sscanf($values, "%1s%1s%1s");

                $this->r = hexdec($r.$r);
                $this->g = hexdec($g.$g);
                $this->b = hexdec($b.$b);

                break;

            case 6;
                list($r, $g, $b) = sscanf($values, "%02x%02x%02x");

                $this->r = $r;
                $this->g = $g;
                $this->b = $b;

                break;
        }

        $this->rgb = [
            $this->r,
            $this->g,
            $this->b
        ];
    }

    public function luma() {
        if(!is_null($this->luma)) {
            return $this->luma;
        }

        $r = $this->r / 255;
        $g = $this->g / 255;
        $b = $this->b / 255;

        return (0.2126 * pow($r, 2.2)) + (0.7152 * pow($g, 2.2)) + (0.0722 * pow($b, 2.2));
    }

    public function rgb() {
        return $this->rgb;
    }

    public function hex() {
        return $this->hex;
    }

    public function __toString() {
        return "rgb({$this->r},{$this->g},{$this->b})";
    }
}