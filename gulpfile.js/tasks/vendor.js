const gulp = require('gulp')
const resolve = require('resolve')
const browserify = require('browserify')
const sourceMaps = require('gulp-sourcemaps')
const buffer = require('vinyl-buffer')
const source = require('vinyl-source-stream')
const rename = require('gulp-rename')
const sass = require('gulp-sass')
const uglify = require('gulp-uglify')

const config = require('../../gulpConfig')
const npm = require('../npm')
const handleError = require('../handleError')

const b = browserify({debug: true})
const cssFiles = []

npm.getPackageIds().forEach(id =>
  b.require(resolve.sync(id, {
    packageFilter: json => {
      if(json.style)
        cssFiles.push(`node_modules/${id}/${json.style}`)
      return json;
    }
  }), {expose: id})
)

gulp.task('vendor-js', (done) => {
  b.bundle()
    .on('error', handleError)
    .pipe(source('vendor.min.js'))
    .pipe(buffer())
    .pipe(sourceMaps.init({loadMaps: true}))
    .pipe(uglify())
    //.pipe(sourceMaps.write('.'))
    .pipe(gulp.dest(config.destRoot + '/js'));

  return done();
});

gulp.task('vendor-css', (done) => {
  gulp.src(cssFiles, {base: 'node_modules'})
    .pipe(sourceMaps.init({loadMaps: true}))
    .pipe(sass({outputStyle: 'compressed'}))
    .on('error', handleError)
    //.pipe(rename('vendor.min.css'))
    //.pipe(sourceMaps.write('.'))
    .pipe(gulp.dest(config.destRoot + '/css'));

  return done();
});

gulp.task('vendor', gulp.parallel('vendor-js', 'vendor-css'))
