const gulp   = require('gulp'),
      gutil  = require('gulp-util'),
      config = require('../../gulpConfig');

/**
 * `create_file()`
 *
 * Takes a filename and string, and writes a file with that name, with the
 * string used as the file’s contents.
 */
function create_file(filename, string) {
    var src = require('stream').Readable({ objectMode: true });

    src._read = function () {
        this.push(new gutil.File({
            cwd: "",
            base: "",
            path: filename,
            contents: new Buffer(string)
        }));

        this.push(null);
    }

    return src;
}

gulp.task('template-styles', () => {
  var pkg = require('../../package.json');

  var style = "";

  style += "/*!\n";

  style += (typeof(pkg.name)        !== 'undefined' ? `Theme Name: ${pkg.name}\n` : '');
  style += (typeof(pkg.homepage)    !== 'undefined' ? `Theme URI: ${pkg.homepage}\n` : '');
  style += (typeof(pkg.description) !== 'undefined' ? `Description: ${pkg.description}\n` : '');
  style += (typeof(pkg.version)     !== 'undefined' ? `Version: 1.0.0 (Starter ${pkg.version})\n` : '');
  style += (typeof(pkg.author)      !== 'undefined' ? `Author: ${pkg.author}\n` : '');
  style += (typeof(pkg.homepage)    !== 'undefined' ? `Author URI: ${pkg.homepage}\n` : '');
  style += (typeof(pkg.keywords)    !== 'undefined' ? `Tags: ${pkg.keywords.join(', ')}\n` : '');
  style += "Template: base-theme\n";

  style += "*/";

  return create_file("style.css", style)
    .pipe(gulp.dest(config.srcRoot))
});
