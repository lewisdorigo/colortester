const gulp        = require('gulp'),
      args        = require('yargs').argv,
      gulpIf      = require('gulp-if'),
      sketch      = require('gulp-sketch'),
      svgSymbols  = require('gulp-svg-symbols'),
      imagemin    = require('gulp-imagemin'),
      rename      = require('gulp-rename'),
      browserSync = require('browser-sync'),
      handleError = require('../handleError'),
      which       = require('npm-which')(__dirname),
      config      = require('../../gulpConfig'),
      svgOptimize = require('../config/svgOptimize.json');




/**
 * SKETCH-ICON
 *
 * Exports to an SVG spritesheet from `/images/sketch/icon.sketch`, outputting
 * to `../assets/icons/`.
 */
gulp.task('sketch-icon', () => {
  return gulp.src(`images/sketch/icons.sketch`)
    .pipe(sketch({
      export      : 'artboards',
      formats     : ['svg'],
      saveFoWeb   : true,
      compact     : true,
    }))
    .pipe(rename(function (path) {
      if(path.dirname !== '.') {
        path.basename = path.dirname+'+'+path.basename;
      }
    }))
    .pipe(imagemin([
        imagemin.svgo({
            plugins: svgOptimize.concat([{ "cleanupIDs" : true }])
        }),
      ]))
    .pipe(svgSymbols({
      fontSize: 16,
      warn: false,
      className: '.icon--%f',
      slug: function(name) {
        return name.replace('+','_');
      },
    }))
    .pipe(gulpIf( /[.]svg$/, rename('icons.svg')))
    .pipe(gulpIf( /[.]svg$/, gulp.dest(config.destRoot)))
    .pipe(gulpIf( /[.]css$/, rename({extname:'.scss'})))
    .pipe(gulpIf( /[.]scss$/, gulp.dest(config.cacheRoot)))
    .pipe(browserSync.reload({stream: true}));
});
