const gulp        = require('gulp'),
      args        = require('yargs').argv,
      gulpIf      = require('gulp-if'),
      sketch      = require('gulp-sketch'),
      svgSymbols  = require('gulp-svg-symbols'),
      imagemin    = require('gulp-imagemin'),
      rename      = require('gulp-rename'),
      browserSync = require('browser-sync'),
      handleError = require('../handleError'),
      which       = require('npm-which')(__dirname),
      config      = require('../../gulpConfig'),
      svgOptimize = require('../config/svgOptimize.json');






/**
 * SKETCH-PINNED
 *
 * Exports SVG icon for pinned tabs from `/images/sketch/pinned.sketch`.
 */
gulp.task('sketch-pinned', () => {
  return gulp.src(`images/sketch/pinned.sketch`)
    .pipe(sketch({
      export      : 'artboards',
      formats     : ['svg'],
      saveFoWeb   : true,
      compact     : true,
      //clean       : true,
    }))
    .pipe(imagemin([
      imagemin.svgo({
          plugins: svgOptimize
      }),
    ]))
    .pipe(rename('pinned.svg'))
    .pipe(gulp.dest(`${config.destRoot}/img`));
});
