const gulp        = require('gulp'),
      args        = require('yargs').argv,
      gulpIf      = require('gulp-if'),
      sketch      = require('gulp-sketch'),
      svgSymbols  = require('gulp-svg-symbols'),
      imagemin    = require('gulp-imagemin'),
      rename      = require('gulp-rename'),
      browserSync = require('browser-sync'),
      handleError = require('../handleError'),
      which       = require('npm-which')(__dirname),
      config      = require('../../gulpConfig');






/**
 * SKETCH-FAVICON
 *
 * Exports PNG based favicons, and a .ico file for older browsers, from
 * `/images/sketch/favicon.sketch`
 */
gulp.task('sketch-favicon', () => {
  return gulp.src(`images/sketch/favicon.sketch`)
    .pipe(sketch({
      export      : 'artboards',
      formats     : ['png'],
      scales      : [1,2,3],
      saveFoWeb   : true,
      compact     : true,
    }))
    .pipe(imagemin([
        imagemin.optipng({
            optimizationLevel: 5,
        }),
      ]))
    .pipe(gulp.dest(`${config.destRoot}/img/favicon`))
});
