const gulp        = require('gulp'),
      args        = require('yargs').argv,
      gulpIf      = require('gulp-if'),
      sketch      = require('gulp-sketch'),
      svgSymbols  = require('gulp-svg-symbols'),
      imagemin    = require('gulp-imagemin'),
      rename      = require('gulp-rename'),
      browserSync = require('browser-sync'),
      handleError = require('../handleError'),
      which       = require('npm-which')(__dirname),
      config      = require('../../gulpConfig'),
      svgOptimize = require('../config/svgOptimize.json');





/**
 * SKETCH-SVG
 *
 * Exports SVGs with PNG fallbacks from `images/sketch/svg.sketch`, outputting
 * to `../assets/svg/`.
 */
gulp.task('sketch-svg', () => {
  return gulp.src(`images/sketch/svg.sketch`)
    .pipe(sketch({
      export      : 'artboards',
      formats     : ['svg', 'png'],
      saveFoWeb   : true,
      compact     : true,
    }))
    .pipe(imagemin([
      imagemin.svgo({
        plugins: svgOptimize.concat([{ "cleanupIDs" : false }])
      }),
      imagemin.optipng({
        optimizationLevel: 5,
      }),
    ]))
    .pipe(gulp.dest(`${config.destRoot}/img`))
    .pipe(browserSync.reload({stream: true}));
});
