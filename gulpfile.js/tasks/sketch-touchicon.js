const gulp        = require('gulp'),
      args        = require('yargs').argv,
      gulpIf      = require('gulp-if'),
      sketch      = require('gulp-sketch'),
      svgSymbols  = require('gulp-svg-symbols'),
      imagemin    = require('gulp-imagemin'),
      rename      = require('gulp-rename'),
      browserSync = require('browser-sync'),
      handleError = require('../handleError'),
      which       = require('npm-which')(__dirname),
      config      = require('../../gulpConfig');








/**
 * SKETCH-TOUCHICON
 *
 * Exports PNG based touch-icons for iOS webclippings from the artboards in
 * `/images/sketch/touchicon.sketch`
 */
gulp.task('sketch-touchicon', () => {
  return gulp.src(`images/sketch/touch-icon.sketch`)
    .pipe(sketch({
      export      : 'artboards',
      formats     : ['png'],
      scales      : [2,3],
      saveFoWeb   : true,
      compact     : true,
    }))
    .pipe(imagemin([
        imagemin.optipng({
            optimizationLevel: 5,
        }),
      ]))
    .pipe(gulp.dest(`${config.destRoot}/img`));
});
