const gulp        = require('gulp'),
      babelify    = require('babelify'),
      browserify  = require('browserify'),
      plumber     = require('gulp-plumber'),
      uglify      = require('gulp-uglify'),
      sourceMaps  = require('gulp-sourcemaps'),
      buffer      = require('vinyl-buffer'),
      source      = require('vinyl-source-stream'),
      browserSync = require('browser-sync'),
      rename      = require('gulp-rename');

const config = require('../../gulpConfig');


gulp.task('js', (done) => {

  return browserify({
      entries: `${config.srcRoot}/index.js`,
      debug: true
    }).transform('babelify')
    .transform('browserify-shim')
    .bundle()
    .pipe(plumber())
    .pipe(source('app.min.js'))
    .pipe(buffer())
    .pipe(sourceMaps.init({loadMaps: true}))
    .pipe(uglify())
    .pipe(sourceMaps.write('.'))
    .pipe(gulp.dest(`${config.destRoot}/js`))
    .pipe(browserSync.reload({stream: true}));
});
