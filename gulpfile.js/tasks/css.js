const gulp         = require('gulp')
const sass         = require('gulp-sass')
const sassGlob     = require('gulp-sass-glob')
const autoPrefixer = require('gulp-autoprefixer')
const args         = require('yargs').argv
const wrap         = require('gulp-wrap')
const base64       = require('gulp-base64')
const sourceMaps   = require('gulp-sourcemaps')
const rename       = require('gulp-rename')
const gulpIf       = require('gulp-if')
const browserSync  = require('browser-sync')
const plumber      = require('gulp-plumber');

const config = require('../../gulpConfig');

gulp.task('css', () => {
  return gulp.src(`${config.srcRoot}/index.scss`)
    .pipe(sourceMaps.init())
    .pipe(wrap(`<%= contents %>
                @import '**/*';`))
    .pipe(sassGlob({
      ignorePaths: [
        'browser.scss'
      ]
    }))
    .pipe(sass({
      sourcemap: true,
      trace: true,
      outputStyle: 'compressed',
    }).on('error', sass.logError))
    .pipe(autoPrefixer({
      grid: true,
      browsers: [
        ">0.25% in GB",
        "not dead"
      ]
    }))
    .pipe(base64({
      baseDir: `${config.destRoot}/css`,
      extensions: ['svg', 'png', 'gif'],
      exclude: ['fonts'],
      maxImageSize: 4 * 1024
    }))
    .pipe(rename('app.min.css'))
    .pipe(sourceMaps.write('maps', {
      includeContent: false,
      sourceRoot: 'sass'
    }))
    .pipe(gulp.dest(`${config.destRoot}/css`))
    .pipe(browserSync.stream({ match: "**/*.css" }));
});