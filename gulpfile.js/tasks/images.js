const gulp        = require('gulp'),
      imagemin    = require('gulp-imagemin'),
      changed     = require('gulp-changed'),
      browserSync = require('browser-sync');

const config = require('../../gulpConfig')

gulp.task('images', () => {
  return gulp.src([
      'images/**/*.{svg,png,jpg,gif}',
    ])
    .pipe(imagemin([
      imagemin.jpegtran({
        progressive: true,
      }),
      imagemin.optipng({
        optimizationLevel: 5,
      }),
    ]))
    .pipe(changed(config.destRoot))
    .pipe(gulp.dest(`${config.destRoot}/img`))
    .pipe(browserSync.reload({stream: true}))
});
