const gulp      = require('gulp'),
      plumber   = require('gulp-plumber'),
      uglify    = require('gulp-uglify'),
      modernizr = require('gulp-modernizr');

const config = require('../../gulpConfig')

/**
 * MODERNIZR
 *
 * Generates a modernizr.js file with the specified tests.
 */
gulp.task('modernizr', (done) => {
	return gulp.src([`${config.destRoot}/js/app.min.js`, `${config.destRoot}/css/app.min.css`])
	  .pipe(plumber())
		.pipe(modernizr({
			cache : true,
			tests: [],
			excludeTests: ["hidden"],
			options: [
				"setClasses"
			]
		}))
		.pipe(uglify())
	  .pipe(plumber.stop())
		.pipe(gulp.dest(`${config.destRoot}/js`));
});
