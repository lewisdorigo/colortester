const gulp        = require('gulp'),
      browserSync = require('browser-sync'),
      args        = require('yargs').argv,
      config      = require('../../gulpConfig');

gulp.task('browsersync', (done) => {
  if (typeof(args.serve) === 'undefined' || args.serve !== false) {
    browserSync({
      proxy: config.domain,
      notify: false,
      logFileChanges: false
    });
  }

  return done();
});
