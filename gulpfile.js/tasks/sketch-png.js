const gulp        = require('gulp'),
      args        = require('yargs').argv,
      gulpIf      = require('gulp-if'),
      sketch      = require('gulp-sketch'),
      svgSymbols  = require('gulp-svg-symbols'),
      imagemin    = require('gulp-imagemin'),
      rename      = require('gulp-rename'),
      browserSync = require('browser-sync'),
      handleError = require('../handleError'),
      which       = require('npm-which')(__dirname),
      config      = require('../../gulpConfig');





/**
 * SKETCH-PNG
 *
 * Exports SVGs with PNG fallbacks from `images/sketch/png.sketch`, outputting
 * to `../assets/img/`.
 */
gulp.task('sketch-png', () => {
  return gulp.src(`images/sketch/png.sketch`)
    .pipe(sketch({
      export    : 'artboards',
      formats   : ['png'],
      scales    : [1,2,3],
      saveFoWeb : true
    }))
    .pipe(imagemin([
      imagemin.optipng({
        optimizationLevel: 5,
      }),
    ]))
    .pipe(gulp.dest(config.destRoot + '/img'))
    .pipe(browserSync.reload({stream: true}));
});
