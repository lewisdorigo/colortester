const gulp = require('gulp'),
      del  = require('del');

const config = require('../../gulpConfig');

gulp.task('clean-del', () => {
  return del([

    // cache
    `${config.cacheRoot}/**`,
    `!${config.cacheRoot}`,
    `!${config.cacheRoot}/.gitkeep`,

    // theme
    `${config.destRoot}/**`,
    `!${config.destRoot}`,
    `!${config.cacheRoot}/.gitkeep`,

  ])
});
