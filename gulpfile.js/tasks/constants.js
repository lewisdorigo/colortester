const gulp    = require('gulp'),
      plumber = require('gulp-plumber'),
      jsonCss = require('gulp-json-css'),
      rename  = require('gulp-rename');

const config = require('../../gulpConfig')

gulp.task('constants', () => {
  return gulp.src(`constants.json`)
    .pipe(jsonCss({
      keepObjects: true,
    }))
    .pipe(rename('constants.scss'))
    .pipe(gulp.dest(`${config.cacheRoot}`))
});
