const gulp        = require('gulp'),
      browserSync = require('browser-sync'),
      config      = require('../../gulpConfig');

gulp.task('watch', (done) => {
  gulp.watch([
    `${config.srcRoot}/**/*.js`,
    `!${config.srcRoot}/assets/**/*.js`,
    `js/**/*.js`,
  ], gulp.series('js','modernizr'));

  gulp.watch([
    `${config.srcRoot}/**/*.{sass,scss}`,
    `scss/**/*.{sass,scss}`
  ], gulp.series('css'));

  gulp.watch([
    `${config.srcRoot}/**/*.php`,
    `${config.srcRoot}/**/*.html`,
  ]).on('change', browserSync.reload);

  gulp.watch(`images/**/*.{svg,png,jpg,gif}`, gulp.series('images'));

  gulp.watch(`images/sketch/icons.sketch`, gulp.series('sketch-icon','css'));
  gulp.watch(`images/sketch/svg.sketch`, gulp.series('sketch-svg'));
  gulp.watch(`images/sketch/png.sketch`, gulp.series('sketch-png'));

  gulp.watch(`images/sketch/touch-icon.sketch`, gulp.series('sketch-touchicon'));
  gulp.watch(`images/sketch/favicon.sketch`, gulp.series('sketch-favicon'));

  gulp.watch(`constants.json`, gulp.parallel('constants','css','js'));

  return done();
});
