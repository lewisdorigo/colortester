const _ = require('lodash')

module.exports = {
  getPackageIds: () => {
    const packageManifest = require(process.cwd() + '/package.json')
    return _.keys(packageManifest.dependencies) || []
  }
}