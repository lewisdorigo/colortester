const gulp = require('gulp');

gulp.task("clean-build", gulp.series("clean-del",
                                     "template-styles",
                                     gulp.parallel("sketch-icon", "sketch-png", "sketch-svg", "sketch-favicon", "sketch-pinned", "sketch-touchicon")));
