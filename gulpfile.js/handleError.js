const notify = require('gulp-notify');

module.exports = () => {
    var args = Array.prototype.slice.call(arguments);
    notify.onError({
        title: 'Gulp Error',
        message: '<%= error.message %>'
    }).apply(this, args);
    this.emit('end');
}