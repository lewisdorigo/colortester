const gulp        = require('gulp'),
      requireDir  = require('require-dir'),
      config      = require('../gulpConfig');

requireDir('./tasks');
requireDir('./defaultTasks');

gulp.task("default", gulp.series("build", "browsersync", "watch"));
gulp.task("clean",   gulp.series("clean-build", "default"));
