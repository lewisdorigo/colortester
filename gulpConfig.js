require('dotenv').config()

module.exports = {
    srcRoot: 'src',
    destRoot: 'www/static',
    cacheRoot: 'gulpfile.js/cache',
    domain: process.env.SITE_URL
}